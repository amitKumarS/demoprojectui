package com.cucumber.Manager;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import enums.DriverType;
import enums.EnvironmentType;

public class WebDriverManager {
	private WebDriver driver;
	private static DriverType driverType;
	private static EnvironmentType environmentType;
	private static final String CHROME_DRIVER_PROPERTY="webdriver.chrome.driver";
	private static final String IE_DRIVER_PROPERTY="webdriver.ie.driver";
	private static String driverpath=System.getProperty("user.dir")+"\\Drivers\\"; 
	String nodeURL="";
	
	public WebDriverManager() {
		driverType = FileReaderManager.getInstance().getConfigReader().getBrowser();
		
	}
	public  WebDriver getDriver() throws MalformedURLException {
		if(driver == null) driver = createDriver();
		return driver;
	}

	private WebDriver createDriver() throws MalformedURLException {
		   switch (environmentType) {	    
	        case LOCAL : driver = createLocalDriver();
	        	break;
	        case REMOTE : driver = createRemoteDriver();
	        	break;
		   }
		   return driver;
	}
	private WebDriver createRemoteDriver() throws MalformedURLException {
		DesiredCapabilities capability;
		 switch (driverType) {	    
	        case FIREFOX : driver = new FirefoxDriver();
		    	break;
	        case CHROME :
	        	capability= DesiredCapabilities.chrome();
	        	driver =  new RemoteWebDriver(new URL(nodeURL), capability);
	    		break;
	        case INTERNETEXPLORER :
	        	capability = DesiredCapabilities.internetExplorer();
	        	driver = new RemoteWebDriver(new URL(nodeURL), capability);
	    		break;
	        }
		//capability.setBrowserName("chrome");
		//capability.setPlatform(Platform.ANY);
		//driver = new RemoteWebDriver(new URL(nodeURL), capability);
		if(FileReaderManager.getInstance().getConfigReader().getBrowserWindowSize()) driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(FileReaderManager.getInstance().getConfigReader().getImplicitlyWait(), TimeUnit.SECONDS);
        driver.manage().deleteAllCookies();
		return driver;
	}
 
	private WebDriver createLocalDriver() {
        switch (driverType) {	    
        case FIREFOX : driver = new FirefoxDriver();
	    	break;
        case CHROME : 
        	System.setProperty(CHROME_DRIVER_PROPERTY, driverpath + "chromedriver.exe");
        	driver = new ChromeDriver();
    		break;
        case INTERNETEXPLORER : 
        	System.setProperty(IE_DRIVER_PROPERTY, driverpath + "IEDriverServer.exe");
        	driver = new InternetExplorerDriver();
    		break;
        }
        if(FileReaderManager.getInstance().getConfigReader().getBrowserWindowSize()) driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(FileReaderManager.getInstance().getConfigReader().getImplicitlyWait(), TimeUnit.SECONDS);
        driver.manage().deleteAllCookies();
		return driver;
	}
	
	public void scrollDriverUp(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,-500)");
	}
	
	public void scrollDriverDown(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,500)");
	}
 
	public void closeDriver() {
		driver.close();
		driver.quit();
	}
	

}
