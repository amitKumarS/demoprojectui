package com.cucumber.test;
import com.cucumber.PageObjects.Consumer;
import cucumber.TestContext;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;


public class ConsumerSteps {

	Consumer consumer;
	TestContext testContext;
	
	@Given("^Open \"([^\"]*)\" site$")
	public void openSite(String site) throws Throwable {
		consumer.navigationTo_URL(site);
	}
	
	
	@And("^Navigate to BT parental Controls$")
	public void btParentalControlPage()throws Throwable{
		consumer.navigateTo_BTParentalControl();
	}

	
	@And("^Validate that URLS for \"([^\"]*)\" categories are blocked$")
	public void validateBlockedURLs(String FilterLevel)throws Throwable{
		consumer.compareTitle(FilterLevel);
	}
	

	
}
